import { ValidationChecks } from 'langium';
import { GuitarAstType } from './generated/ast';
import type { GuitarServices } from './guitar-module';

/**
 * Register custom validation checks.
 */
export function registerValidationChecks(services: GuitarServices) {
    const registry = services.validation.ValidationRegistry;
    const validator = services.validation.GuitarValidator;
    const checks: ValidationChecks<GuitarAstType> = {
        //Person: validator.checkPersonStartsWithCapital
    };
    registry.register(checks, validator);
}

/**
 * Implementation of custom validations.
 */
export class GuitarValidator {

    

}
