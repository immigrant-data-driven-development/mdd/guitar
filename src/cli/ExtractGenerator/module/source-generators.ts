import fs from "fs";
import { expandToString } from "langium";
import { createPath } from '../../generator-utils'
import { Application, Configuration,isLocalEntity, isModule, LocalEntity } from "../../../language/generated/ast";
import path from 'path'

export function generateSource(application: Application, target_folder: string) : void {
    fs.mkdirSync(target_folder, {recursive:true})
    const EXTRACT_PATH = createPath(target_folder, "extract_component")
    
    const SRC_PATH = createPath(EXTRACT_PATH, "src")
    const SERVICE_PATH = createPath(SRC_PATH, "services")

    let entities: Array<LocalEntity> = []

    for (const module of application.abstractElements.filter(isModule)){
        for (const entity of module.elements.filter(isLocalEntity)){
            entities.push(entity)
        }
    }

    fs.writeFileSync(path.join(SRC_PATH, '__init__.py'),"")
    fs.writeFileSync(path.join(SRC_PATH, 'application.py'),generateApplication(entities))

    if (application.configuration){
        fs.writeFileSync(path.join(SERVICE_PATH, '__init__.py'),"")    
        fs.writeFileSync(path.join(SERVICE_PATH, 'extract.py'),generateExtract(application.configuration, entities))
        fs.writeFileSync(path.join(SERVICE_PATH, 'transform.py'),generateTransform(application.configuration, entities))
        fs.writeFileSync(path.join(SERVICE_PATH, 'fn.py'),generateFunction())
        fs.writeFileSync(path.join(SERVICE_PATH, 'util.py'),generateUtil())
    }


}

function generateUtil(){
    return expandToString`
    import json
    import datetime
    class Util:
        def __json_default(self,value):
            """ Convert a specific object to dict 
            
            Args:
                object value: a object to convert to dict
            """
            if isinstance(value, datetime.date):
                return dict(year=value.year, month=value.month, day=value.day)
            else:
                return value.__dict__

        def object_to_json(self, entity):
            return json.dumps(entity,ensure_ascii=False,default = lambda o: self.__json_default(o), sort_keys=True, indent=4).encode('utf-8')

        def object_to_dict(self, entity):
            """ Convert an object to dict 
            
            Args:
                object entity: a entity to convert to dict
            """
            json_entity = self.object_to_json(entity)
            return json.loads(json_entity)
    `
}

function generateTransform(configuration:Configuration, entities: Array<LocalEntity>):string{
    return expandToString`
    from apache_beam.transforms import PTransform
    import apache_beam as beam
    from .fn import FnExtract, FnExtractByProject, FnTuple
    from beam_nuggets.io import kafkaio
    from decouple import config

    class TransformRetrieveProject(PTransform):
        def __init__(self):
            self.application = "gitlab"
            self.entities = ['project']
            self.producer_config = {'bootstrap.servers': config('SERVERS')}

        def expand(self, pcoll):
            result = None
            for entity in self.entities:
                result = (
                    pcoll
                    | "Retrieve {}".format(entity) >>beam.ParDo(FnExtract(function_name=entity))    
                )
                    
            return result
    
    class TransformPublish(PTransform):
        def __init__(self, entity = None):
            self.application = "gitlab"
            self.producer_config = {'bootstrap.servers': config('SERVERS')}
            self.entity = entity
        
        def expand(self, pcoll):
            result = (
                    pcoll
                    | "Transform in Tuple {}".format( self.entity) >>beam.ParDo(FnTuple())
                    | "Sending  {} to Kafka".format( self.entity) >> kafkaio.KafkaProduce(topic="application.{}.{}".format(self.application, self.entity),servers=config('SERVERS'))        
                )
                        
            return result 
    
    class TransformByProject(PTransform):
     
        def __init__(self):
            self.application = "${configuration.software_name ?? 'Software Name not configured'} "
            self.entities = [${entities.map(entity => `${entity.name.toLowerCase()},`)}]
            self.producer_config = {'bootstrap.servers': config('SERVERS')}
        
        def expand(self, pcoll):
            result = None
            for entity in self.entities:
                result = (
                    pcoll
                    | "Retrive all {} from a project".format(entity) >>beam.ParDo(FnExtractByProject(function_name=entity)) 
                    | "Transform in Tuple {} from a project".format(entity) >>beam.ParDo(FnTuple())
                    | "Sending  {} to Kafka from a project".format(entity) >> kafkaio.KafkaProduce(topic="application.{}.{}".format(self.application,entity),servers=config('SERVERS'))        
                )
                        
            return result        
    
    class TransformByProjectEntity(PTransform):
     
        def __init__(self, entity, details = False):
            self.application = "gitlab"
            self.entity = entity
            self.details= details
            self.producer_config = {'bootstrap.servers': config('SERVERS')}
        
        def expand(self, pcoll):
               
            result = (
                    pcoll
                    | "Retrive all {} from a project".format(self.entity) >>beam.ParDo(FnExtractByProject(function_name=self.entity,details= self.details)) 
                    | "Transform in Tuple {} from a project".format(self.entity) >>beam.ParDo(FnTuple())
                    | "Sending  {} to Kafka from a project".format(self.entity) >> kafkaio.KafkaProduce(topic="application.{}.{}".format(self.application,self.entity),servers=config('SERVERS'))        
                )
                        
            return result
    
    class Transform(PTransform):
        def __init__(self):
            self.application = "${configuration.software_name ?? 'Software Name not configured'} "
            self.entities = [${entities.map(entity => `${entity.name.toLowerCase()},`)}]
            self.producer_config = {'bootstrap.servers': config('SERVERS')}
            
        def expand(self, pcoll):
            result = None
            for entity in self.entities:
                result = (
                    pcoll
                    | "Retrieve {}".format(entity) >>beam.ParDo(FnExtract(function_name=entity))
                    | "Transform in Tuple {}".format(entity) >>beam.ParDo(FnTuple())
                    | "Sending  {} to Kafka".format(entity) >> kafkaio.KafkaProduce(topic="application.{}.{}".format(self.application,entity),servers=config('SERVERS'))
                    
                )
            return result
    
    `
}
function generateFunction():string{
    return expandToString`
    import apache_beam as beam
    from pprint import pprint
    from .extract import Extract
    import json
    import uuid 
    from .util import Util

    class FnExtractOnlyProject(beam.DoFn):
    """ Abstract Function"""
        def __init__(self) -> None:
            super().__init__()
            self.application_entity = Extract()
            self.util = Util()
            self.function_name = 'project'
      
        def process(self, element):

            try:
           
                data = json.loads(element[1])
              
                self.application_entity.config (
                    entity = self.function_name,
                    personal_access_token = data['secret'],
                    gitlab_url = data['gitlab_url'],
                    organization_uuid = data['configuration_uuid'],
                    configuration_uuid = data['organization_uuid']
                )
            
                result = self.application_entity.do()
                return result
            except Exception as e:
                    print (e)


    class FnExtractByProject(beam.DoFn):
    """ Abstract Function"""
        def __init__(self,function_name, details = False) -> None:
            super().__init__()
            self.application_entity = Extract()
            self.util = Util()
            self.details =  details
            self.function_name = function_name
            
      
        def process(self, element):
            try:
              
                self.application_entity.config (
                    entity = self.function_name,
                    personal_access_token = element['secret'],
                    gitlab_url = element['gitlab_url'],
                    organization_uuid = element['configuration_uuid'],
                    configuration_uuid = element['organization_uuid']
                )
                result = self.application_entity.do_by_project(element['id'],self.details)   
                return result
            except Exception as e:
                    print (e)


    class FnExtract(beam.DoFn):
    
        """ Abstract Function"""
        def __init__(self,function_name) -> None:
            super().__init__()
            self.application_entity = Extract()
            self.util = Util()
            self.function_name = function_name
      
      
        def process(self, element):

            try:
            
                data = json.loads(element[1])
                
                self.application_entity.config (
                    entity = self.function_name,
                    personal_access_token = data['secret'],
                    gitlab_url = data['gitlab_url'],
                    organization_uuid = data['configuration_uuid'],
                    configuration_uuid = data['organization_uuid']
                )
            
                result = self.application_entity.do()      
                return result
            except Exception as e:
                    print (e)
           
    class FnTuple(beam.DoFn):

        def __init__(self) -> None:
            super().__init__()
            self.util = Util()
      
        def process(self, element):
            
            index = str(uuid.uuid4())
            element = json.dumps (element)
            return [(index,element)]
`
}

function generateExtract(configuration:Configuration, entities: Array<LocalEntity>):string{
    return expandToString`
    import logging
    from ${configuration.extract_lib?? `Extract Lib not configured`} import factories
    logging.basicConfig(level=logging.INFO)
    from .util import Util
    import uuid
    class Extract():
    """Abstract Class with the main function used to extract data application and save in a MongoDB"""

    def __init__(self):
        self.util = Util()
        self.instance = None
        
    def config (self, entity, personal_access_token, gitlab_url, organization_uuid,configuration_uuid) :
        
        self.entity = entity
        self.personal_access_token = personal_access_token
        self.gitlab_url = gitlab_url
        self.organization_uuid = organization_uuid
        self.configuration_uuid = configuration_uuid
        
        extract = {
            ${entities.map(entity =>`${entity.name.toLowerCase()}:factories.${entity.name}Factory(),`)}    
        }
        
        self.instance = extract[self.entity]
        
    def do_by_project(self, project_id, details = False):
        """Main function to retrieve and save data in a MongoDB's collection
        
        Args:
            dict data: credentials (secret, url etc) to connect a application
        
        """
        try:
            logging.info("Start Retrieve Information")
            data_extracted =  None

            if self.entity == 'commits' and details:      
                data_extracted = self.instance.get_by_project(project_id,details = True, diff = True)      
            else:
                data_extracted = self.instance.get_by_project(project_id)  
            
            data_transformed = []
            for data in data_extracted:
                
                data_dict = self.util.object_to_dict(data)
                data_dict['entity'] = self.entity
                data_dict['configuration_uuid'] =  self.configuration_uuid 
                data_dict['organization_uuid'] =  self.organization_uuid 
                data_dict['secret'] =  self.personal_access_token
                data_dict['gitlab_url'] = self.gitlab_url 
                data_dict['internal_uuid'] = str(uuid.uuid4())
                data_transformed.append (data_dict)
            
            logging.info("End Returning")
            
            return data_transformed
            
        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)

    def do(self):
        """Main function to retrieve and save data in a MongoDB's collection
        
        Args:
            dict data: credentials (secret, url etc) to connect a application
        
        """
        try:
            logging.info("Start Retrieve Information")
            data_extracted =  None
            data_extracted = self.instance.get_all(today=False)  
            data_transformed = []
            for data in data_extracted:
                
                data_dict = self.util.object_to_dict(data)
                data_dict['entity'] = self.entity
                data_dict['configuration_uuid'] =  self.configuration_uuid 
                data_dict['organization_uuid'] =  self.organization_uuid 
                data_dict['secret'] =  self.personal_access_token
                data_dict['gitlab_url'] = self.gitlab_url 
                data_dict['internal_uuid'] = str(uuid.uuid4())
                data_transformed.append (data_dict)
            
            logging.info("End Returning")
            
            return data_transformed
            
        except Exception as e: 
            logging.error("OS error: {0}".format(e))
            logging.error(e.__dict__)    


    def __json_default(self,value):
        """ Convert a specific object to dict 
        
        Args:
            object value: a object to convert to dict
        """
        if isinstance(value, datetime.date):
            return dict(year=value.year, month=value.month, day=value.day)
        else:
            return value.__dict__

    def object_to_dict(self, entity):
        """ Convert an object to dict 
        
        Args:
            object entity: a entity to convert to dict
        """
        json_entity = json.dumps(entity,ensure_ascii=False,default = lambda o: self.__json_default(o), sort_keys=True, indent=4).encode('utf-8')
        return json.loads(json_entity)
     
    `
}

function generateApplication (entities: Array<LocalEntity>):string{
    return expandToString`
    import apache_beam as beam
    from beam_nuggets.io import kafkaio
    from decouple import config
    from services.transform import TransformPublish,TransformByProjectEntity, TransformRetrieveProject
    import logging

    logging.basicConfig(level=logging.INFO)

    consumer_config = {"topic": config('TOPIC'),
                    "bootstrap_servers": config('SERVERS'),
                    "group_id": config('GROUP_ID'),}

    with beam.Pipeline(runner='DirectRunner') as pipeline:

        credential = (pipeline|  "Reading messages from Kafka"  >> kafkaio.KafkaConsume(consumer_config=consumer_config))

        projects = credential | "Extract Data from atomic entities and Send To Kafka" >> TransformRetrieveProject() 

        projects | "Publish Project Information" >> TransformPublish(entity="project") 
        
        ${entities.map(entity => `projects | "Extract Data from project dependency entites and Send To Kafka: member" >> TransformByProjectEntity('${entity.name.toLowerCase()}')`).join("\n")}
        
    `
}